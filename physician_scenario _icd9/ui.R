library(shiny)

source("data/biome_simulator.R")
input_ids=unique(PtData$ID)
vars_icd=read.table("data/icdgrpMClab.tsv", sep="\t")
#vars_labs=colnames(PtData)[1:372]

# Define the overall UI
shinyUI(fluidPage(    
    titlePanel(img(src="title.png", height=50, width=750)),
    sidebarLayout(      
      sidebarPanel(
        selectInput("patient", "Select patient:", 
                    choices=input_ids),
        selectInput("icd_group",
                    label="ICD Group:",
                    choices=as.character(vars_icd$V1),
                    selected=as.character(vars_icd$V1[1])),
        selectInput("cols", "# of columns:", 
                    choices=c(1,2,3,4)),
        wellPanel(
          checkboxGroupInput("inCheckboxGroup",
                             "Select Labs:",
                             c("label 1" = "option1",
                               "label 2" = "option2"))),
        helpText("Data Updated every 60 seconds"),
        width=2),
      # Create a spot for the barplot
      mainPanel(
       plotOutput("tempPlot",width=1400, height=800)#,
        #plotOutput("tempPlot2",width=800, height=400)
      )
      
    )
))
