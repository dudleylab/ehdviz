library("data.table")

start.time=Sys.time()

load("data/sim.pts.Rdata")

pt.h$timing=(start.time-60*60*36+15*60*(1:300))
pt.l$timing=(start.time-60*60*36+15*60*(1:300))
pt.a$timing=(start.time-60*60*36+15*60*(1:300))
pt.h$location="ED"
pt.l$location="ED"
pt.a$location="ED"
pt.h$location[50:300]="Inpatient_unit_A"
pt.h$location[200:225]="ICU"
pt.a$location[25:300]="Inpatient_unit_A"
pt.a$location[c(50:100,225:250)]="ICU"
pt.l$location[75:300]="Inpatient_unit_A"
pt.l$location[250:length(pt.l$location)]="ICU"
pt.h$ID="John_Doe"
pt.l$ID="John_Snow"
pt.a$ID="Anne_Frost"

PtData=rbind(pt.h,pt.l,pt.a)

rm(pt.h,pt.a,pt.l)

PtData=data.table(PtData)
