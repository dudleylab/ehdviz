library(shiny)

source("data/biome_simulator.R")
input_ids=unique(PtData$location)
vars=colnames(PtData)[1:372]

# Define the overall UI
shinyUI(
  
  # Use a fluid Bootstrap layout
  fluidPage(    
    
  # Give the page a title
     titlePanel(
       img(src="title.png", height=50, width=750)
       ),
    
    # Generate a row with a sidebar
    sidebarLayout(      
      
      # Define the sidebar with one input
      sidebarPanel(
        selectInput("patient", "Select patient:", 
                    choices=input_ids),
        selectInput("cols", "# of columns:", 
                    choices=c(1,2,3,4)),
        checkboxGroupInput("checkGroup",
                           label=h3("EMR Data"),
                           choices=vars,
                           selected=vars[1]),
        hr(),
        helpText("Data Updated every 60 seconds"), width=2
      ),
      
      # Create a spot for the barplot
      mainPanel(
        plotOutput("tempPlot",width=1400, height=800)#,
        #plotOutput("tempPlot2",width=800, height=400)
      )
      
    )
  )
)
